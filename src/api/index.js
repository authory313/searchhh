export default class api {
    static get(url) {
        return fetch(url).then(res => res.json())
    }
}