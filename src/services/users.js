import api from "../api";

const url = 'https://api.github.com/search/users'

export function get_users(
    login,
    per_page,
    order,
    page
) {
    return api.get(`${url}?q=${login}+in:login&sort=repositories&order=${order}&per_page=${per_page}&page=${page}`)
}