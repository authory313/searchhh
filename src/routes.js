import HomePage from './views/HomePage';
import InfoPage from "./views/InfoPage.vue";



const routes = [
    {
        path: '/users',
        name: 'Home',
        component: HomePage
    },
    {
        path: '/',
        redirect: '/users'
    },
    {
        path: '/users/:login',
        name: 'InfoPage',
        component: InfoPage
    },
    {
        path: '*',
        name: 'ErrPage',
        component: () => import('./views/ErrorPage.vue')
    }
];

export default routes;
